package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo/middleware"
	"github.com/gobuffalo/buffalo/middleware/csrf"
	"github.com/gobuffalo/buffalo/middleware/i18n"
	"github.com/gobuffalo/buffalo/middleware/ssl"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/packr"
	"github.com/rs/cors"
	"github.com/unrolled/secure"
	// "gitlab.com/obiknows/caregiver_connect_center/models"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App

// T is a translator
var T *i18n.Translator

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:         ENV,
			SessionName: "_caregiver_connect_center_session",
			PreWares: []buffalo.PreWare{
				cors.Default().Handler,
			},
		})
		// Automatically redirect to SSL
		// app.Use(forceSSL())
		c := cors.New(cors.Options{
			AllowedOrigins: []string{
				"https://caregiverconnectcenter.org",
				"http://caregiverconnectcenter.org",
				"https://www.caregiverconnectcenter.org",
				"http://www.caregiverconnectcenter.org",
				// "http://localhost:3000",
			},
			AllowCredentials: true,
		})
		app.PreWares = append(app.PreWares, c.Handler)

		if ENV == "development" {
			app.Use(middleware.ParameterLogger)
		}

		// Protect against CSRF attacks. https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
		// Remove to disable this.
		app.Use(csrf.New)

		// Wraps each request in a transaction.
		//  c.Value("tx").(*pop.PopTransaction)
		// Remove to disable this.
		// app.Use(middleware.PopTransaction(models.DB))

		// Setup and use translations:
		app.Use(translations())

		app.GET("/", EntryHandler)
		// app.POST("/entry", EntryCallback)
		app.GET("/home", HomeHandler)
		app.GET("/about", AboutHandler)
		app.GET("/forum", ForumHandler)
		// app.GET("/survey", SurveyHandler)
		app.GET("/resources", ResourcesHandler)
		app.GET("/contact", ContactHandler)
		app.GET("/finished", FinishedHandler)
		app.GET("/success", SuccessHandler)
		app.GET("/routes", RoutesHandler)

		app.ServeFiles("/", assetsBox) // serve files from the public directory
	}

	return app
}

// translations will load locale files, set up the translator `actions.T`,
// and will return a middleware to use to load the correct locale for each
// request.
// for more information: https://gobuffalo.io/en/docs/localization
func translations() buffalo.MiddlewareFunc {
	var err error
	if T, err = i18n.New(packr.NewBox("../locales"), "en-US"); err != nil {
		app.Stop(err)
	}
	return T.Middleware()
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return ssl.ForceSSL(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
	})
}
