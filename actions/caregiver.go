package actions

import (
	"time"

	"github.com/gobuffalo/buffalo"
)

// SuccessHandler handles the success condition
func SuccessHandler(c buffalo.Context) error {
	c.Cookies().SetWithExpirationTime("access_cookie", "true", time.Now().Add(30*24*time.Hour))

	return c.Render(200, r.HTML("finished/finished.html"))
}

// EntryHandler is a default handler to serve up
// a home page.
func EntryHandler(c buffalo.Context) error {
	// set entry nav on entry page and set cookie for first time user
	c.Set("entry-nav", true)

	// check if coming as a second time user  (finished cookie is set)
	// if survey has been taken, dont show again
	isFinished, _ := c.Cookies().Get("finished_cookie")
	hasAccess, _ := c.Cookies().Get("access_cookie")

	if len(isFinished) > 0 {
		// finished cookie is set, survey is taken, dont allow click
		c.Set("survey-taken", true)

		return c.Render(200, r.HTML("entry.html"))

	} else if len(hasAccess) > 0 {
		// access cookie is set, survey is taken, and access granted
		c.Set("survey-taken", true)
		// redirect to the home page
		return c.Redirect(302, "/home")

	}

	// any other case, just go to entry
	return c.Render(200, r.HTML("entry.html"))

}

// HomeHandler is a default handler to serve up
// a home page.
func HomeHandler(c buffalo.Context) error {
	// if sent to home, you've successfully completed the survey.
	c.Cookies().SetWithExpirationTime("access_cookie", "true", time.Now().Add(30*24*time.Hour))

	return c.Render(200, r.HTML("home.html"))
}

// AboutHandler is a handler to serve up theh
func AboutHandler(c buffalo.Context) error {
	return c.Render(200, r.HTML("about/about.html"))
}

// SurveyHandler is a handler to serve up theh
func SurveyHandler(c buffalo.Context) error {
	// if survey has been taken, dont show again
	isFinished, err := c.Cookies().Get("finished_cookie")

	if err != nil {
		// not taken cookie, go to survey
		return c.Render(200, r.HTML("survey/survey.html"))

	} else if len(isFinished) > 0 {
		// finished cookie is set, survey is taken, dont allow click
		c.Set("survey-taken", true)

		return c.Render(200, r.HTML("survey/survey.html"))
	} else {
		// any other case, just go to entry
		return c.Render(200, r.HTML("survey/survey.html"))
	}

}

// ResourcesHandler is a handler to serve up
func ResourcesHandler(c buffalo.Context) error {
	return c.Render(200, r.HTML("resources/resources.html"))
}

// ContactHandler is a handler to serve up
func ContactHandler(c buffalo.Context) error {
	return c.Render(200, r.HTML("contact/contact.html"))
}

// FinishedHandler is a handler to serve up
func FinishedHandler(c buffalo.Context) error {
	// set the entry nav in the view
	c.Set("entry-nav", true)

	// on redirect from survey (answering 13), set finished cookie
	c.Cookies().SetWithExpirationTime("finished_cookie", "true", time.Now().Add(30*24*time.Hour))

	// auto redirect

	return c.Render(200, r.HTML("finished/finished.html"))
}

// ForumHandler is a handler to serve up
func ForumHandler(c buffalo.Context) error {
	// on forum, check if the survey has been finished
	finished, err := c.Cookies().Get("finished_cookie")
	// if so,
	if err != nil {
		// no consent cookie, go to denied
		return c.Render(200, r.HTML("forum/denied.html"))
	} else if len(finished) > 0 {
		// consent cookie is set, redirect
		c.Set("survey-taken", true)
		return c.Render(200, r.HTML("forum/forum.html"))
	} else {
		// any other case, just go to entry
		return c.Render(200, r.HTML("forum/denied.html"))
	}
}

// RoutesHandler is a handler to serve up
func RoutesHandler(c buffalo.Context) error {
	return c.Render(200, r.HTML("routes.html"))
}
