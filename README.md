caregiver connect center
========================

Current Prod. Release: obiknows/caregiver_connect_center:v13
Current Dev Release: obiknows/caregiver_connect_center:v14

## Deploy Steps

Currently: this site is deployed on a cloud provider using Docker.

The command below runs the site **without** https

```
# old
docker run --rm -d -p 3000:80 obiknows/caregiver_connect_center:v14
```

```
# new

// get up and running on port 3000
docker run --rm -d -p 3000:3000 obiknows/caregiver_connect_center:v14

// run proxy server that will connect 3000 to ports 80/443 with https
docker run -d \
    -v $(pwd)/Caddyfile:/etc/Caddyfile \
    -v $HOME/.caddy:/root/.caddy \
    -p 80:80 -p 443:443 \
    abiosoft/caddy
```

In order to run with https, must use a proxy server, 


## Database Setup

It looks like you chose to set up your application using a postgres database! Fantastic!

The first thing you need to do is open up the "database.yml" file and edit it to use the correct usernames, passwords, hosts, etc... that are appropriate for your environment.

You will also need to make sure that **you** start/install the database of your choice. Buffalo **won't** install and start postgres for you.

### Create Your Databases

Ok, so you've edited the "database.yml" file and started postgres, now Buffalo can create the databases in that file for you:

	$ buffalo db create -a

## Starting the Application

Buffalo ships with a command that will watch your application and automatically rebuild the Go binary and any assets for you. To do that run the "buffalo dev" command:

	$ buffalo dev

If you point your browser to [http://127.0.0.1:3000](http://127.0.0.1:3000) you should see a "Welcome to Buffalo!" page.

**Congratulations!** You now have your Buffalo application up and running.

## What Next?

We recommend you heading over to [http://gobuffalo.io](http://gobuffalo.io) and reviewing all of the great documentation there.

Good luck!

[Powered by Buffalo](http://gobuffalo.io)
