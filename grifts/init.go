package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/obiknows/caregiver_connect_center/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
